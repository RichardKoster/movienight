package ui;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;

public class Button extends TextView implements View.OnClickListener {

    OnClickListener _l;

    public Button(Context context) {
        this(context, null);
    }

    public Button(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public Button(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        _l.onClick(view);
    }

    public interface OnClickListener {
        void onClick(View v);
    }

    public void setOnClickListener(OnClickListener listener) {
        _l = listener;
    }
}
