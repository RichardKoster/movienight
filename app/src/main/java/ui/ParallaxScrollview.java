package ui;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ScrollView;

public class ParallaxScrollview extends ScrollView {

    OnScrollListener _l;

    public ParallaxScrollview(Context context) {
        this(context, null);
    }

    public ParallaxScrollview(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ParallaxScrollview(Context context, AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);
    }

    public ParallaxScrollview(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @Override
    protected void onScrollChanged(int l, int t, int oldl, int oldt) {
        super.onScrollChanged(l, t, oldl, oldt);
        if (_l != null) {
            _l.onScroll(l, t, oldl, oldt);
        }
    }

    public void setOnScrollListener(OnScrollListener listener) {
        _l = listener;
    }

    public interface OnScrollListener {
        void onScroll(int l, int t, int oldl, int oldt);
    }
}
