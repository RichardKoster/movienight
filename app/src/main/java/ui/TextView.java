package ui;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;

import com.richardkoster.movienight.R;
import com.richardkoster.movienight.app.MnApp;
import com.richardkoster.movienight.ui.typography.FontWeight;


public class TextView extends android.widget.TextView {
    protected FontWeight _fontWeight;

    public TextView(Context context) {
        this(context, null);
    }

    public TextView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public TextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        if (isInEditMode()) {
            return;
        }

        TypedArray attributes = context.getTheme().obtainStyledAttributes(
                attrs,
                R.styleable.TextView,
                0, 0);

        int weight = attributes.getInteger(R.styleable.TextView_fontWeight, 400);
        setFontWeight(weight);
        updateTypeface();
    }

    public void setFontWeight(int weight) {
        setFontWeight(FontWeight.valueOf(weight));
    }

    public void setFontWeight(FontWeight weight) {
        _fontWeight = weight;
        updateTypeface();
    }

    protected void updateTypeface() {
        Typeface typeface = MnApp.getFontManager().getTypeface(_fontWeight);
        setTypeface(typeface, typeface.getStyle());
    }
}
