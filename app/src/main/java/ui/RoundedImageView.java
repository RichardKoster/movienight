package ui;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.util.AttributeSet;
import android.widget.ImageView;

public class RoundedImageView extends ImageView {

    protected Paint _croppingPaint = new Paint();
    protected Paint _basicPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
    protected Paint _borderPaint = new Paint();
    protected Bitmap _tempBitmap;
    protected Canvas _tempCanvas;

    public RoundedImageView(Context context) {
        this(context, null);
    }

    public RoundedImageView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public RoundedImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);

        setCropToPadding(true);

        _basicPaint.setColor(Color.BLUE);

        _croppingPaint.setColor(Color.RED);
        _croppingPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));

        _borderPaint.setStyle(Paint.Style.STROKE);
        _borderPaint.setAntiAlias(true);

        setBorder(.5f, BorderPosition.Inside, Color.argb(51, 0, 0, 0));
    }

    public void drawImage(Canvas canvas) {
        super.draw(canvas);
    }

    @Override
    public void draw(Canvas canvas) {
        float centerX = canvas.getWidth() / 2f;
        float centerY = canvas.getHeight() / 2f;
        float radius = (canvas.getWidth() - getPaddingLeft() - getPaddingRight()) / 2f;

        // We're creating a temp bitmap/canvas because the target one isn't transparent.
        _tempBitmap = Bitmap.createBitmap(canvas.getWidth(), canvas.getHeight(), Bitmap.Config.ARGB_8888);
        _tempCanvas = new Canvas(_tempBitmap);

        _tempCanvas.drawCircle(centerX, centerY, radius, _basicPaint);

        Bitmap contentBitmap = Bitmap.createBitmap(canvas.getWidth(), canvas.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas contentCanvas = new Canvas(contentBitmap);
        drawImage(contentCanvas);

        _tempCanvas.drawBitmap(contentBitmap, 0f, 0f, _croppingPaint);

        _tempCanvas.drawCircle(centerX, centerY, radius, _borderPaint);

        canvas.drawBitmap(_tempBitmap, 0f, 0f, _basicPaint);
    }

    public void setBorder(float width, BorderPosition position, int color) {
        _borderPaint.setStrokeWidth(getResources().getDisplayMetrics().scaledDensity * width * 2f);
        _borderPaint.setColor(color);

        switch (position) {
            case Inside:
                _borderPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_ATOP));
                break;
            case Outside:
                _borderPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_ATOP));
                break;
        }

        invalidate();
    }

    public enum BorderPosition {
        Inside,
        Outside
    }

}
