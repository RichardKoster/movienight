package com.richardkoster.movienight.api.models;

import com.google.gson.annotations.SerializedName;
import com.richardkoster.movienight.trakt.models.entities.users.UserSettings;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.Collections;
import java.util.List;

public class Movienight {

    @SerializedName("id")
    private int _id;
    @SerializedName("title")
    private String _title;
    @SerializedName("location")
    private String _location;
    @SerializedName("users")
    private List<UserSettings.User> _users;
    @SerializedName("movies")
    private List<String> _movieSlugs;
    @SerializedName("date")
    private String _date;

    public Movienight() {
        _location = "Kevin's";
        _users = Collections.emptyList();
        _movieSlugs = Collections.emptyList();
    }

    public Movienight(String title, String location, List<UserSettings.User> users, List<String> movieSlugs, String date){
        _title = title;
        _location = location;
        _users = users;
        _movieSlugs = movieSlugs;
        _date = date;
    }

    public boolean hasMultiple(){
        if(_movieSlugs.size()>1){
            return true;
        }
        return false;
    }

    public DateTime getDate(){
        DateTimeFormatter f = DateTimeFormat.forPattern("MM-dd-yyyy");
        return f.parseDateTime(_date);
    }

    public String getTitle(){
        return _title;
    }

    public String getLocation(){
        return _location;
    }

    public List<UserSettings.User> getUsers(){
        return _users;
    }

}
