package com.richardkoster.movienight.api.helper.models;

import com.google.gson.annotations.SerializedName;

public class CreateUserModel {

    private String username;
    private String name;
    private String location;
    @SerializedName("avatar_url")
    private String avatarUrl;

    public CreateUserModel(String username, String name, String location, String avatarUrl) {
        this.username = username;
        this.name = name;
        this.location = location;
        this.avatarUrl = avatarUrl;
    }
}
