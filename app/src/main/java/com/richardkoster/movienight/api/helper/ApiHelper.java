package com.richardkoster.movienight.api.helper;

import android.net.Uri;

import com.google.gson.Gson;
import com.richardkoster.movienight.api.helper.models.CreateUserModel;
import com.richardkoster.movienight.trakt.models.entities.users.UserSettings;
import com.richardkoster.movienight.trakt.models.auth.AccessToken;

public class ApiHelper {

    private static Gson _gson;

    public ApiHelper() {
        _gson = new Gson();
    }

    public static CreateUserModel getUserCreatePostdata(UserSettings.User user, AccessToken token) {
        String url = Uri.encode(user.getAvatarUrl());

        return new CreateUserModel(user.getUsername(), user.getName(), user.getLocation(), url);
    }
}
