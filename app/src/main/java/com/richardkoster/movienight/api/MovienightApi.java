package com.richardkoster.movienight.api;

import com.richardkoster.movienight.api.helper.models.CreateUserModel;
import com.richardkoster.movienight.api.models.Movienight;

import retrofit.Callback;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.client.Response;
import retrofit.http.Body;
import retrofit.http.POST;

public class MovienightApi {

    private final static String BASE_URL = "http://movienight.richardkoster.com/api";
    private final static String RB_BASE = "http://requestb.in";
    private final static String RB_URL = "/q320jlq3";

    public static MovienightController getApi() {
        RestAdapter adapter = new RestAdapter.Builder()
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setEndpoint(BASE_URL)
                .setRequestInterceptor(new RequestInterceptor() {
                    @Override
                    public void intercept(RequestFacade request) {
                        request.addHeader("Content-type", "application/json");
                    }
                })
                .build();

        return adapter.create(MovienightController.class);
    }

    public interface MovienightController {

        @POST("/user/create")
        public void addUser(@Body CreateUserModel postdata, Callback<Response> cb);

        @POST("/movienight/create")
        public void addMovienight(@Body Movienight movienight, Callback<Movienight> result);

    }
}
