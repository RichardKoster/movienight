package com.richardkoster.movienight;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

import com.richardkoster.movienight.api.models.Movienight;
import com.richardkoster.movienight.app.MnApp;
import com.richardkoster.movienight.ui.UserGroupView;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import butterknife.ButterKnife;
import butterknife.InjectView;
import ui.TextView;

public class MovienightDetailActivity extends AppCompatActivity {

    @InjectView(R.id.title)
    TextView _title;
    @InjectView(R.id.location)
    TextView _location;
    @InjectView(R.id.date)
    TextView _date;
    @InjectView(R.id.usergroup)
    UserGroupView _userGroup;

    MnApp _app;

    Movienight _mn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movienight_detail);
        ButterKnife.inject(this);
        getWindow().setStatusBarColor(0x00ffffff);

        _app = (MnApp) getApplication();
        _mn = _app.getCurrentMovienight();
        if(_mn!=null){ populate(); }
        else{ finish(); }
    }

    public void populate(){
        _title.setText(_mn.getTitle());
        _location.setText(String.format("At %s", _mn.getLocation()));
        DateTime dt = _mn.getDate();
        DateTimeFormatter f = DateTimeFormat.forPattern("dd MMMM");
        _date.setText(f.print(dt));
        _userGroup.setUserList(_mn.getUsers());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_movienight_detail, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
