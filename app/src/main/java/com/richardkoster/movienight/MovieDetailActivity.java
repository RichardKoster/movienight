package com.richardkoster.movienight;

import android.graphics.Bitmap;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.graphics.Palette;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.Toast;

import com.getbase.floatingactionbutton.FloatingActionButton;
import com.getbase.floatingactionbutton.FloatingActionsMenu;
import com.richardkoster.movienight.app.MnApp;
import com.richardkoster.movienight.trakt.api.TraktApi;
import com.richardkoster.movienight.trakt.models.entities.movies.HistorySync;
import com.richardkoster.movienight.trakt.models.entities.movies.Movie;
import com.richardkoster.movienight.trakt.models.entities.movies.WatchlistSync;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import butterknife.ButterKnife;
import butterknife.InjectView;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import ui.EllipsizingTextView;
import ui.ParallaxScrollview;
import ui.TextView;

public class MovieDetailActivity extends AppCompatActivity implements ParallaxScrollview.OnScrollListener {

    @InjectView(R.id.title)
    TextView _title;
    @InjectView(R.id.meta)
    TextView _meta;
    @InjectView(R.id.overview)
    EllipsizingTextView _overview;
    @InjectView(R.id.poster)
    ImageView _poster;
    @InjectView(R.id.rating)
    RatingBar _rating;
    @InjectView(R.id.parallax_scrollview)
    ParallaxScrollview _scrollView;
    @InjectView(R.id.toolbar)
    Toolbar _toolbar;
    @InjectView(R.id.fab_add)
    FloatingActionsMenu _fabMenu;

    MnApp _app;

    Movie _movie;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_detail);
        getWindow().setStatusBarColor(0x00ffffff);
        ButterKnife.inject(this);
        _app = (MnApp) getApplication();
        _movie = _app.getCurrentMovie();

        if(_movie.getGenres()==null){
            TraktApi.getApi(_app).getMovie(_movie.getIds()._traktSlug, new Callback<Movie>() {
                @Override
                public void success(Movie movie, Response response) {
                    _movie = movie;
                    populate();
                }

                @Override
                public void failure(RetrofitError error) {

                }
            });
        }
        else{
            populate();
        }

        FloatingActionButton newMovieNightWithMovie = new FloatingActionButton(getBaseContext());
        newMovieNightWithMovie.setSize(FloatingActionButton.SIZE_NORMAL);
        newMovieNightWithMovie.setTitle("Plan new movienight");
        _fabMenu.addButton(newMovieNightWithMovie);
        final FloatingActionButton changeMovieStatus = new FloatingActionButton(getBaseContext());
        changeMovieStatus.setSize(FloatingActionButton.SIZE_MINI);
        setButtonMovieStatus(changeMovieStatus, _movie.getStatus());

        changeMovieStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (_movie.getStatus()) {
                    case Movie.NOTHING:
                        addMovieToWatchlist();
                        _movie.setStatus(Movie.WATCHLIST);
                        break;
                    case Movie.WATCHLIST:
                        setMovieWatched();
                        _movie.setStatus(Movie.WATCHED);
                        break;
                    case Movie.WATCHED:
                        removeFromWatched();
                        _movie.setStatus(Movie.WATCHLIST);
                        break;
                }
                setButtonMovieStatus(changeMovieStatus, _movie.getStatus());
            }
        });
        _fabMenu.addButton(changeMovieStatus);

        _fabMenu.setOnFloatingActionsMenuUpdateListener(new FloatingActionsMenu.OnFloatingActionsMenuUpdateListener() {
            @Override
            public void onMenuExpanded() {
                if(_movie.getStatus()==Movie.WATCHLIST){
                    aDrawable.start();
                }

            }

            @Override
            public void onMenuCollapsed() {

            }
        });



        _scrollView.setOnScrollListener(this);
        _toolbar.setTitle("");
        setSupportActionBar(_toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public void setMovieWatched(){
        HistorySync hSync = new HistorySync();
        hSync.setSyncHistoryMovie(new HistorySync.SyncHistoryMovie(_movie.getIds()));
        TraktApi.getApi(_app).addToWatched(hSync, new Callback<retrofit.client.Response>() {
            @Override
            public void success(retrofit.client.Response response, retrofit.client.Response response2) {
                Toast.makeText(getBaseContext(), "Done", Toast.LENGTH_SHORT).show();
            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(getBaseContext(), error.getLocalizedMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    public void addMovieToWatchlist(){
        WatchlistSync wSync = new WatchlistSync();
        wSync.setSyncHistoryMovie(new WatchlistSync.WatchlistSyncMovie(_movie.getIds()));
        TraktApi.getApi(_app).addToWatchlist(wSync, new Callback<Response>() {
            @Override
            public void success(Response response, Response response2) {
                Toast.makeText(getBaseContext(), "Done", Toast.LENGTH_SHORT).show();

            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(getBaseContext(), error.getLocalizedMessage(), Toast.LENGTH_LONG).show();

            }
        });
    }

    public void removeFromWatched(){
        WatchlistSync rSync = new WatchlistSync();
        rSync.setSyncHistoryMovie(new WatchlistSync.WatchlistSyncMovie(_movie.getIds()));
        TraktApi.getApi(_app).removeFromHistory(rSync, new Callback<Response>() {
            @Override
            public void success(Response response, Response response2) {

            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
    }

    AnimationDrawable aDrawable;

    public void setButtonMovieStatus(FloatingActionButton b, int status){
        switch(status){
            case Movie.NOTHING:
                b.setColorNormal(getResources().getColor(R.color.primaryDark));
                b.setTitle("Add to watchlist");
                break;
            case Movie.WATCHLIST:
                b.setColorNormal(getResources().getColor(R.color.colorPrimary));
                aDrawable = (AnimationDrawable) getResources().getDrawable(R.drawable.ic_eye, getTheme());
                b.setIconDrawable(aDrawable);
                b.setTitle("Set movie as watched");
                break;
            case Movie.WATCHED:
                b.setColorNormal(0xffF44336);
                b.setIconDrawable(getResources().getDrawable(R.mipmap.ic_delete_white_24dp, getTheme()));
                b.setTitle("Remove from watched history");
                break;
        }
    }

    public void populate() {
        _title.setText(_movie.getTitle());
        _meta.setText(_movie.getMetaString());
        _overview.setText(_movie.getOverview());
        Picasso.with(getApplicationContext()).load(_movie.getImages().getPoster().getMedium()).into(_target);
        if (!(_movie.getRating() == -1)) {
            Double rating = _movie.getRating();
            _rating.setProgress(rating.intValue());
            _rating.setContentDescription("Rating: " + rating.intValue() + " out of 10");
            _rating.setVisibility(View.VISIBLE);
        } else {
            _rating.setVisibility(View.GONE);
        }

    }

    Target _target = new Target() {
        @Override
        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
            Palette palette = Palette.generate(bitmap);
            Palette.Swatch lightVibrantSwatch = palette.getLightVibrantSwatch();
            Palette.Swatch lightMutedSwatch = palette.getLightMutedSwatch();
            Palette.Swatch swatch = lightVibrantSwatch;
            if (lightVibrantSwatch == null) {
                swatch = lightMutedSwatch;
            }
            _title.setTextColor(swatch.getRgb());
            _poster.setImageBitmap(bitmap);
        }

        @Override
        public void onBitmapFailed(Drawable errorDrawable) {

        }

        @Override
        public void onPrepareLoad(Drawable placeHolderDrawable) {

        }
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_movie_detail, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        switch (id) {
            case R.id.action_settings:
                return true;
            case android.R.id.home:
                finish();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onScroll(int l, int t, int oldl, int oldt) {
        int scrollY = _scrollView.getScrollY();

        _poster.setTop(-scrollY / 2);
    }
}
