package com.richardkoster.movienight.fragments;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.richardkoster.movienight.MovieDetailActivity;
import com.richardkoster.movienight.R;
import com.richardkoster.movienight.adapters.SearchAdapter;
import com.richardkoster.movienight.app.MnApp;
import com.richardkoster.movienight.trakt.api.TraktApi;
import com.richardkoster.movienight.trakt.models.entities.movies.Movie;
import com.richardkoster.movienight.trakt.models.search.SearchResult;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class SearchFragment extends Fragment{

    @InjectView(R.id.list)
    RecyclerView _list;

    MnApp _app;

    SearchAdapter _adapter;

    List<Movie> _movies;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);
        return inflater.inflate(R.layout.fragment_search, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.inject(this, view);

        _app = (MnApp) getActivity().getApplication();

        _list.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));

        _adapter = new SearchAdapter();
        _list.setAdapter(_adapter);

        _adapter.setOnItemTappedListener(new SearchAdapter.OnItemTappedListener() {
            @Override
            public void onTap(RecyclerView.ViewHolder vh, int position) {
                _app.setCurrentMovie(_movies.get(position));
                startActivity(new Intent(getActivity(), MovieDetailActivity.class));
            }
        });
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        MenuItem item = menu.findItem(R.id.action_search);
        if(item!=null){
            SearchView sv = (SearchView) item.getActionView();
            sv.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                @Override
                public boolean onQueryTextSubmit(String query) {
                    if (query.contains(":")){
                        String titlequery = query.substring(0, query.indexOf(":"));
                        String yearQuery = query.substring(query.indexOf(":")+1);

                        TraktApi.getApi(_app).searchForMovieWithYear(titlequery, TraktApi.SEARCH_MOVIE, yearQuery, new Callback<ArrayList<SearchResult.MovieResult>>() {
                            @Override
                            public void success(ArrayList<SearchResult.MovieResult> results, Response response) {
                                List<Movie> movies = SearchResult.getMovieList(results);
                                _adapter.updateDataset(movies);
                                _movies = movies;
                            }

                            @Override
                            public void failure(RetrofitError error) {

                            }
                        });
                    }
                    else{
                        TraktApi.getApi(_app).searchForMovie(query, TraktApi.SEARCH_MOVIE, new Callback<ArrayList<SearchResult.MovieResult>>() {
                            @Override
                            public void success(ArrayList<SearchResult.MovieResult> movieResults, Response response) {
                                List<Movie> movies = SearchResult.getMovieList(movieResults);
                                _adapter.updateDataset(movies);
                                _movies = movies;
                            }

                            @Override
                            public void failure(RetrofitError error) {

                            }
                        });
                    }

                    Log.d("Search", query);
                    return false;
                }

                @Override
                public boolean onQueryTextChange(String newText) {
                    Log.d("Search", newText);
                    return false;
                }
            });
        }

    }

}
