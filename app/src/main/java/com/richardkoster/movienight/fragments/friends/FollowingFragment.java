package com.richardkoster.movienight.fragments.friends;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.richardkoster.movienight.R;
import com.richardkoster.movienight.adapters.FriendsAdapter;
import com.richardkoster.movienight.adapters.viewholders.FriendViewHolder;
import com.richardkoster.movienight.app.MnApp;
import com.richardkoster.movienight.trakt.api.TraktApi;
import com.richardkoster.movienight.trakt.models.entities.users.FriendContainer;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class FollowingFragment extends Fragment {

    @InjectView(R.id.list)
    RecyclerView _list;

    FriendsAdapter _adapter;

    MnApp _app;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_watchlist, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.inject(this, view);
        _app = (MnApp) getActivity().getApplication();

        _list.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        _adapter = new FriendsAdapter(new FriendsAdapter.OnItemTappedListener() {
            @Override
            public void onTap(FriendViewHolder vh, int position) {

            }
        });
        _list.setAdapter(_adapter);

        TraktApi.getApi(_app).getFollowingForUser(_app.getUserSettings().getUser().getUsername(), new Callback<List<FriendContainer>>() {
            @Override
            public void success(List<FriendContainer> friendContainers, Response response) {
                _adapter.updateDataset(friendContainers);
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
    }
}
