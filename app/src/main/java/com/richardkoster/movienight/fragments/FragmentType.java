package com.richardkoster.movienight.fragments;

public enum FragmentType {

    Movienights,
    Lists,
    Friends,
    Settings,
    Watchlist,
    Watched,
    FriendsList,
    Following,
    Followers,
    Search
}
