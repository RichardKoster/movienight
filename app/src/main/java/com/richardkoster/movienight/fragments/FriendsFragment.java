package com.richardkoster.movienight.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.richardkoster.movienight.DrawerActivity;
import com.richardkoster.movienight.R;
import com.richardkoster.movienight.fragments.friends.FollowersFragment;
import com.richardkoster.movienight.fragments.friends.FollowingFragment;
import com.richardkoster.movienight.fragments.friends.FriendsListFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import it.neokree.materialtabs.MaterialTab;
import it.neokree.materialtabs.MaterialTabHost;
import it.neokree.materialtabs.MaterialTabListener;

public class FriendsFragment extends Fragment implements MaterialTabListener {

    @InjectView(R.id.tabhost)
    MaterialTabHost _tabhost;
    @InjectView(R.id.viewpager)
    ViewPager _viewPager;

    ArrayList<ListsFragment.TabItem> _tabItems;
    FriendsViewPagerAdapter _adapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_lists, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.inject(this, view);

        _tabItems = new ArrayList<>();
        _tabItems.add(new ListsFragment.TabItem("Friends", FragmentType.Friends));
        _tabItems.add(new ListsFragment.TabItem("Following", FragmentType.Following));
        _tabItems.add(new ListsFragment.TabItem("Followers", FragmentType.Followers));

        _adapter = new FriendsViewPagerAdapter(((DrawerActivity) getActivity()).getSupportFragmentManager(), _tabItems);
        _viewPager.setAdapter(_adapter);
        _viewPager.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                _tabhost.setSelectedNavigationItem(position);
            }
        });

        for (int i = 0; i < _tabItems.size(); i++) {
            ListsFragment.TabItem item = _tabItems.get(i);

            _tabhost.addTab(_tabhost.createCustomTextTab(R.layout.tab_item, item.getTitle(), true).setTabListener(this));
//            _tabhost.addTab(_tabhost.newTab()
//                    .setText(item.getTitle())
//                    .setTabListener(this));
        }

    }

    @Override
    public void onTabSelected(MaterialTab materialTab) {
        _viewPager.setCurrentItem(materialTab.getPosition());
    }

    @Override
    public void onTabReselected(MaterialTab materialTab) {

    }

    @Override
    public void onTabUnselected(MaterialTab materialTab) {

    }

    public class FriendsViewPagerAdapter extends FragmentStatePagerAdapter {

        List<ListsFragment.TabItem> _tabitems;

        public FriendsViewPagerAdapter(FragmentManager fm, List<ListsFragment.TabItem> tabItems) {
            super(fm);
            _tabitems = tabItems;
        }

        @Override
        public android.support.v4.app.Fragment getItem(int position) {
            ListsFragment.TabItem tab = _tabitems.get(position);
            switch (tab.getType()) {
                case FriendsList:
                    return new FriendsListFragment();
                case Following:
                    return new FollowingFragment();
                case Followers:
                    return new FollowersFragment();
                default:
                    return new FriendsListFragment();
            }
        }

        @Override
        public int getCount() {
            return _tabitems.size();
        }
    }

}
