package com.richardkoster.movienight.fragments.lists;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.richardkoster.movienight.MovieDetailActivity;
import com.richardkoster.movienight.R;
import com.richardkoster.movienight.adapters.MovieContainerAdapter;
import com.richardkoster.movienight.adapters.viewholders.MovieViewHolder;
import com.richardkoster.movienight.app.MnApp;
import com.richardkoster.movienight.trakt.api.TraktApi;
import com.richardkoster.movienight.trakt.models.entities.movies.Movie;
import com.richardkoster.movienight.trakt.models.entities.movies.MovieContainer;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class WatchlistFragment extends android.support.v4.app.Fragment {

    @InjectView(R.id.list)
    RecyclerView _list;

    MnApp _app;
    String _userName;

    MovieContainerAdapter _movieAdapter;

    List<MovieContainer> _movies;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_watchlist, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.inject(this, view);

        _app = (MnApp) getActivity().getApplication();
        _userName = _app.getUserSettings().getUser().getUsername();

        _list.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));

        _movieAdapter = new MovieContainerAdapter(new MovieContainerAdapter.OnItemTappedListener() {
            @Override
            public void onClick(MovieViewHolder vh, int position) {
                _app.setCurrentMovie(_movies.get(position).getMovie());
                Intent intent = new Intent(getActivity(), MovieDetailActivity.class);
                startActivity(intent);
            }
        });
        _list.setAdapter(_movieAdapter);

        getWatchlistMovies();
    }

    void getWatchlistMovies(){
        TraktApi.getApi(_app).getWatchlistForUser(_userName, new Callback<List<MovieContainer>>() {
            @Override
            public void success(List<MovieContainer> movieContainers, Response response) {
                for (int i = 0; i < movieContainers.size(); i++) {
                    movieContainers.get(i).getMovie().setStatus(Movie.WATCHLIST);
                }
                _movieAdapter.updateDataset(movieContainers);
                _movies = movieContainers;
            }

            @Override
            public void failure(RetrofitError error) {
                Toast.makeText(getActivity(), error.getLocalizedMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

}
