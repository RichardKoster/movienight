package com.richardkoster.movienight.fragments;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.richardkoster.movienight.MovienightDetailActivity;
import com.richardkoster.movienight.R;
import com.richardkoster.movienight.adapters.MovienightsAdapter;
import com.richardkoster.movienight.adapters.viewholders.MovienightViewholder;
import com.richardkoster.movienight.api.models.Movienight;
import com.richardkoster.movienight.app.MnApp;
import com.richardkoster.movienight.trakt.models.entities.users.UserSettings;

import java.util.ArrayList;
import java.util.List;


public class MovienightsFragment extends Fragment implements MovienightsAdapter.OnItemTappedListener {

    MovienightsAdapter _adapter;

    MnApp _app;
    List<Movienight> movienightList;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_movienights, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        _app = (MnApp) getActivity().getApplication();

        _adapter = new MovienightsAdapter();
        LinearLayoutManager m = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        RecyclerView _list = (RecyclerView) view;
        _list.setLayoutManager(m);
        _list.setAdapter(_adapter);
        movienightList = new ArrayList<>();
        for (int i = 0; i < 10; i++) {
            ArrayList<UserSettings.User> users = new ArrayList<>();
            for(int j = 0; i<3; i++){
                users.add(_app.getUserSettings().getUser());
            }
            ArrayList<String> movieSlugs = new ArrayList<>();
            movieSlugs.add("interstellar-2014");
            Movienight movienight = new Movienight("Interstellar night","Kevin's", users, movieSlugs, "08-01-2015");
            movienightList.add(movienight);
        }
        _adapter.updateDataset(movienightList);

        _adapter.setOnItemTappedListener(this);
    }

    @Override
    public void onItemTapped(MovienightViewholder vh, int position) {
        if(movienightList.size()!=0){
            _app.setCurrentMovienight(movienightList.get(position));
            startActivity(new Intent(getActivity(), MovienightDetailActivity.class));
        }

    }
}
