package com.richardkoster.movienight.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.richardkoster.movienight.DrawerActivity;
import com.richardkoster.movienight.R;
import com.richardkoster.movienight.fragments.lists.WatchedFragment;
import com.richardkoster.movienight.fragments.lists.WatchlistFragment;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import it.neokree.materialtabs.MaterialTab;
import it.neokree.materialtabs.MaterialTabHost;
import it.neokree.materialtabs.MaterialTabListener;

public class ListsFragment extends Fragment implements MaterialTabListener {

    @InjectView(R.id.tabhost)
    MaterialTabHost _tabHost;
    @InjectView(R.id.viewpager)
    ViewPager _viewPager;

    ListsPagerAdapter _pagerAdapter;

    ArrayList<TabItem> _tabItems;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_lists, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ButterKnife.inject(this, view);
        _tabItems = new ArrayList<>();
        _tabItems.add(new TabItem("Watchlist", FragmentType.Watchlist));
        _tabItems.add(new TabItem("Watched", FragmentType.Watched));
        _pagerAdapter = new ListsPagerAdapter(((DrawerActivity) getActivity()).getSupportFragmentManager(), _tabItems);
        _viewPager.setAdapter(_pagerAdapter);
        _viewPager.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                super.onPageSelected(position);
                _tabHost.setSelectedNavigationItem(position);
            }
        });

        for (int i = 0; i < _tabItems.size(); i++) {
            _tabHost.addTab(_tabHost.createCustomTextTab(R.layout.tab_item, _tabItems.get(i).getTitle(), true).setTabListener(this));

//            _tabHost.addTab(
//                    _tabHost.newTab()
//                            .setText(_pagerAdapter.getPageTitle(i))
//                            .setTabListener(this)
//            );
        }
    }

    @Override
    public void onTabSelected(MaterialTab materialTab) {
        _viewPager.setCurrentItem(materialTab.getPosition());
    }

    @Override
    public void onTabReselected(MaterialTab materialTab) {

    }

    @Override
    public void onTabUnselected(MaterialTab materialTab) {

    }

    public class ListsPagerAdapter extends FragmentStatePagerAdapter {

        public List<TabItem> tabItems;

        public ListsPagerAdapter(FragmentManager fm, List<TabItem> tabItems) {
            super(fm);
            this.tabItems = tabItems;
        }

        @Override
        public android.support.v4.app.Fragment getItem(int position) {
            TabItem item = tabItems.get(position);
            switch (item.getType()) {
                case Watchlist:
                    return new WatchlistFragment();
                case Watched:
                    return new WatchedFragment();
                default:
                    return new WatchlistFragment();
            }
        }

        @Override
        public int getCount() {
            return tabItems.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return tabItems.get(position).getTitle();
        }
    }

    public static class TabItem {

        private String _title;
        private FragmentType _type;

        public TabItem(String title, FragmentType type) {
            _title = title;
            _type = type;
        }

        public String getTitle() {
            return _title;
        }

        public FragmentType getType() {
            return _type;
        }
    }
}
