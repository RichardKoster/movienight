package com.richardkoster.movienight.ui.typography;

class Font {
    protected final String _filename;
    protected final FontWeight _weight;
    protected final FontStyle _style;

    public Font(String filename, FontWeight weight) {
        this(filename, weight, FontStyle.Normal);
    }

    public Font(String filename, FontWeight weight, FontStyle style) {
        _filename = filename;
        _weight = weight;
        _style = style;
    }

    public int getDescription() {
        return _weight.getDescription() + _style.getDescription();
    }

    public String getFilename() {
        return _filename;
    }

}
