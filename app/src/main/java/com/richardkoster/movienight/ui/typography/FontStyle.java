package com.richardkoster.movienight.ui.typography;

public enum FontStyle {
    Normal(0),
    Italic(1);

    protected final int _description;

    FontStyle(int description) {
        _description = description;
    }

    public int getDescription() {
        return _description;
    }
}
