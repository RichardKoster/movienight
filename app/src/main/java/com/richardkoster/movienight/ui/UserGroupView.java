package com.richardkoster.movienight.ui;

import android.content.Context;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.richardkoster.movienight.trakt.models.entities.users.UserSettings;
import com.squareup.picasso.Picasso;

import java.util.List;

import ui.RoundedImageView;

public class UserGroupView extends LinearLayout{

    public UserGroupView(Context context) {
        this(context, null);
    }

    public UserGroupView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public UserGroupView(Context context, AttributeSet attrs, int defStyleAttr) {
        this(context, attrs, defStyleAttr, 0);
    }

    public UserGroupView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        setOrientation(LinearLayout.HORIZONTAL);
    }

    public void setUserList(List<UserSettings.User> users){
        DisplayMetrics m = getResources().getDisplayMetrics();
        for (UserSettings.User user : users) {
            RoundedImageView v = new RoundedImageView(getContext());
            v.setLayoutParams(new ViewGroup.LayoutParams((int) (40*m.scaledDensity), (int) (40*m.scaledDensity)));
            v.setPadding(0, 0, 8, 0);
            v.setScaleType(ImageView.ScaleType.CENTER_CROP);
            Picasso.with(getContext()).load(user.getAvatarUrl()).into(v);
            addView(v);
        }
    }
}
