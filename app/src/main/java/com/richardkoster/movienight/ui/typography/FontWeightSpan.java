package com.richardkoster.movienight.ui.typography;

import android.graphics.Paint;
import android.os.Parcel;
import android.text.ParcelableSpan;
import android.text.TextPaint;
import android.text.style.MetricAffectingSpan;

import com.richardkoster.movienight.app.MnApp;


public class FontWeightSpan extends MetricAffectingSpan implements ParcelableSpan {

    private final FontWeight _weight;

    public FontWeightSpan(FontWeight weight) {
        _weight = weight;
    }

    public FontWeightSpan(Parcel src) {
        _weight = FontWeight.valueOf(src.readInt());
    }

    @Override
    public int getSpanTypeId() {
        return 0;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(_weight.getDescription());
    }

    @Override
    public void updateDrawState(TextPaint ds) {
        apply(ds, _weight);
    }

    @Override
    public void updateMeasureState(TextPaint paint) {
        apply(paint, _weight);
    }

    private static void apply(Paint paint, FontWeight weight) {
        paint.setTypeface(MnApp.getFontManager().getTypeface(weight));
    }

}
