package com.richardkoster.movienight.ui.typography;

import android.content.Context;
import android.graphics.Typeface;

public class RobotoFontManager extends FontManager {

    protected Font[] _fonts = new Font[]{
            new Font("Roboto-Thin.ttf", FontWeight.Thin),
            new Font("Roboto-ThinItalic.ttf", FontWeight.Thin, FontStyle.Italic),
            new Font("Roboto-Light.ttf", FontWeight.Light),
            new Font("Roboto-LightItalic.ttf", FontWeight.Light, FontStyle.Italic),
            new Font("Roboto-Regular.ttf", FontWeight.Regular),
            new Font("Roboto-Italic.ttf", FontWeight.Regular, FontStyle.Italic),
            new Font("Roboto-Medium.ttf", FontWeight.Medium),
            new Font("Roboto-MediumItalic.ttf", FontWeight.Medium, FontStyle.Italic),
            new Font("Roboto-Bold.ttf", FontWeight.Bold),
            new Font("Roboto-BoldItalic.ttf", FontWeight.Bold, FontStyle.Italic),
            new Font("Roboto-Black.ttf", FontWeight.Black),
            new Font("Roboto-BlackItalic.ttf", FontWeight.Black, FontStyle.Italic)
    };

    public RobotoFontManager(Context context) {
        super(context);
    }

    protected Typeface findTypeface(FontWeight weight, FontStyle style) {
        Font font = null;
        int description = weight.getDescription() + style.getDescription();

        for (Font f : _fonts) {
            if (f.getDescription() == description) {
                font = f;
                break;
            }
        }

        if (font == null) {
            return null;
        }

        return Typeface.createFromAsset(getContext().getAssets(), "fonts/" + font.getFilename());
    }

}
