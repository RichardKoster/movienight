package com.richardkoster.movienight.ui.typography;

import android.content.Context;
import android.graphics.Typeface;

public class DebugFontManager extends FontManager {

    protected Typeface _debugTypeface;

    public DebugFontManager(Context context) {
        super(context);
    }

    @Override
    protected Typeface findTypeface(FontWeight weight, FontStyle style) {
        if (_debugTypeface == null) {
            _debugTypeface = Typeface.createFromAsset(getContext().getAssets(), "fonts/Chalkduster.ttf");
        }

        return _debugTypeface;
    }
}
