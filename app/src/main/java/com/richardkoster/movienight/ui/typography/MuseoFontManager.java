package com.richardkoster.movienight.ui.typography;

import android.content.Context;
import android.graphics.Typeface;

public class MuseoFontManager extends FontManager {

    protected Font[] _fonts = new Font[]{
            new Font("Museo-Sans-300.otf", FontWeight.Light),
            new Font("Museo-Sans-500.otf", FontWeight.Regular),
            new Font("Museo-Sans-700.otf", FontWeight.Medium),
    };

    public MuseoFontManager(Context context) {
        super(context);
    }

    protected Typeface findTypeface(FontWeight weight, FontStyle style) {
        Font font = null;
        int description = weight.getDescription() + style.getDescription();

        for (Font f : _fonts) {
            if (f.getDescription() == description) {
                font = f;
                break;
            }
        }

        if (font == null) {
            return null;
        }

        return Typeface.createFromAsset(getContext().getAssets(), "fonts/" + font.getFilename());
    }
}
