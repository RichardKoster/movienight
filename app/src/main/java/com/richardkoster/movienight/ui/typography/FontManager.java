package com.richardkoster.movienight.ui.typography;

import android.content.Context;
import android.graphics.Typeface;
import android.util.SparseArray;

public abstract class FontManager {

    protected Context _context;
    protected SparseArray<Typeface> _typefaces = new SparseArray<>();

    public FontManager(Context context) {
        _context = context;
    }

    public Typeface getTypeface(FontWeight weight) {
        return getTypeface(weight, FontStyle.Normal);
    }

    public Typeface getTypeface(FontWeight weight, FontStyle style) {
        int value = weight.getDescription() + style.getDescription();

        if (_typefaces.get(value) == null) {
            _typefaces.put(value, findTypeface(weight, style));
        }

        return _typefaces.get(value);
    }

    protected Context getContext() {
        return _context;
    }

    protected abstract Typeface findTypeface(FontWeight weight, FontStyle style);

}
