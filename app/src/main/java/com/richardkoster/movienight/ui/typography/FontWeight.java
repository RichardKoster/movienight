package com.richardkoster.movienight.ui.typography;

public enum FontWeight {
    Thin(100),
    Light(300),
    Regular(400),
    Medium(500),
    Bold(700),
    Black(900);

    private final int _description;

    FontWeight(int description) {
        _description = description;
    }

    public int getDescription() {
        return _description;
    }

    public static FontWeight valueOf(int description) {
        for (FontWeight weight : FontWeight.values()) {
            if (weight.getDescription() == description) {
                return weight;
            }
        }

        return null;
    }
}
