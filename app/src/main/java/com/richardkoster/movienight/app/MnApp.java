package com.richardkoster.movienight.app;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.util.Log;

import com.google.gson.Gson;
import com.richardkoster.movienight.api.MovienightApi;
import com.richardkoster.movienight.api.helper.models.CreateUserModel;
import com.richardkoster.movienight.api.models.Movienight;
import com.richardkoster.movienight.trakt.api.TraktApi;
import com.richardkoster.movienight.trakt.models.entities.users.UserSettings;
import com.richardkoster.movienight.trakt.models.auth.AccessToken;
import com.richardkoster.movienight.trakt.models.entities.movies.Movie;
import com.richardkoster.movienight.ui.typography.FontManager;
import com.richardkoster.movienight.ui.typography.MuseoFontManager;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import timber.log.Timber;

public class MnApp extends Application {

    private final static String TOKEN_KEY = "token";
    private final static String USER_KEY = "user";

    private static MnApp _app;
    private static FontManager _fontManager;

    private static SharedPreferences _sharedPrefs;
    private static SharedPreferences.Editor _editor;

    static Gson _gson;

    static AccessToken _token;
    static UserSettings _userSettings;

    private OnSavedUserInfoListener _l;

    @Override
    public void onCreate() {
        super.onCreate();
        _app = this;

        _sharedPrefs = getSharedPreferences("com.richardkoster.movienight", Context.MODE_PRIVATE);
        _editor = _sharedPrefs.edit();

        _gson = new Gson();

        Timber.plant(new Timber.DebugTree());
    }

    public static Context getContext() {
        return _app.getApplicationContext();
    }

    public static FontManager getFontManager() {
        if (_fontManager == null) {
            _fontManager = new MuseoFontManager(getContext());
        }

        return _fontManager;
    }

    public void setAccessToken(final AccessToken token) {
        String tokenString = _gson.toJson(token);
        _token = token;
        _editor.putString(TOKEN_KEY, tokenString);
        _editor.commit();

        final String userString = _sharedPrefs.getString(USER_KEY, "");
        if (userString == "") {
            TraktApi.getApi(this).getCurrentUser(new Callback<UserSettings>() {
                @Override
                public void success(final UserSettings userSettings, Response response) {
                    _userSettings = userSettings;
                    //MovienightApi.getApi().addUser(userSettings.getUser().getUsername(), userSettings.getUser().getName(), token.getAccessToken(), userSettings.getUser().getLocation(), userSettings.getUser().getAvatarUrl(), new Callback<Response>() {
                    MovienightApi.getApi().addUser(new CreateUserModel(userSettings.getUser().getUsername(), userSettings.getUser().getName(), userSettings.getUser().getLocation(), Uri.encode(userSettings.getUser().getAvatarUrl())), new Callback<Response>() {
                        @Override
                        public void success(Response s, Response response) {
                            Log.d("Successsss", response.getReason());
                            storeUser(userSettings);
                            _l.didSaveUser();
                        }

                        @Override
                        public void failure(RetrofitError error) {
                            Log.d("Successsss", error.getLocalizedMessage());
                        }
                    });

                }

                @Override
                public void failure(RetrofitError error) {
                    Log.d("Retro", error.getLocalizedMessage());
                }
            });
        }
    }

    private void storeUser(UserSettings userSettings) {
        String userString = _gson.toJson(userSettings);
        _editor.putString(USER_KEY, userString);
        _editor.commit();
    }

    public AccessToken getAccessToken() {
        String tokenString = _sharedPrefs.getString(TOKEN_KEY, "");
        return _gson.fromJson(tokenString, AccessToken.class);
    }

    public UserSettings getUserSettings() {
        String userString = _sharedPrefs.getString(USER_KEY, "");
        return _gson.fromJson(userString, UserSettings.class);
    }

    public interface OnSavedUserInfoListener {
        void didSaveUser();
    }

    public void setOnSavedUserInfoListener(OnSavedUserInfoListener listener) {
        _l = listener;
    }

    public boolean hasAccessToken() {
        String token = _sharedPrefs.getString(TOKEN_KEY, "");
        if (token.equals("")) {
            return false;
        }
        return true;
    }

    public void logOut() {
        _editor.putString(TOKEN_KEY, "");
        _editor.putString(USER_KEY, "");
        _editor.commit();
    }

    Movie currentMovie;

    public void setCurrentMovie(Movie movie) {
        currentMovie = movie;
    }

    public Movie getCurrentMovie() {
        return currentMovie;
    }

    Movienight _mn;
    public void setCurrentMovienight(Movienight mn){
        _mn = mn;
    }

    public Movienight getCurrentMovienight(){
        return _mn;
    }

}
