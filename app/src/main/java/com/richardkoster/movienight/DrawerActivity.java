package com.richardkoster.movienight;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.SpannableString;
import android.text.Spanned;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ListView;

import com.richardkoster.movienight.adapters.MenuAdapter;
import com.richardkoster.movienight.app.MnApp;
import com.richardkoster.movienight.fragments.CreateMovienightFragment;
import com.richardkoster.movienight.fragments.FragmentType;
import com.richardkoster.movienight.fragments.FriendsFragment;
import com.richardkoster.movienight.fragments.ListsFragment;
import com.richardkoster.movienight.fragments.MovienightsFragment;
import com.richardkoster.movienight.fragments.SearchFragment;
import com.richardkoster.movienight.fragments.SettingsFragment;
import com.richardkoster.movienight.trakt.models.entities.users.UserSettings;
import com.richardkoster.movienight.utils.TypefaceSpan;
import com.squareup.picasso.Picasso;

import butterknife.ButterKnife;
import butterknife.InjectView;
import ui.RoundedImageView;
import ui.TextView;


public class DrawerActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {

    @InjectView(R.id.drawer)
    DrawerLayout _drawer;
    @InjectView(R.id.content_frame)
    FrameLayout _contentFrame;
    @InjectView(R.id.menu)
    ListView _menu;
    @InjectView(R.id.toolbar)
    Toolbar _toolbar;

    FragmentType _currentFragment = FragmentType.Movienights;

    int _loginRequestCode = 100;

    MenuAdapter.MenuItem[] items = new MenuAdapter.MenuItem[]{
            new MenuAdapter.MenuItem("Movienights", R.mipmap.ic_drawer_movienights, FragmentType.Movienights),
            new MenuAdapter.MenuItem("Lists", R.mipmap.ic_drawer_lists, FragmentType.Lists),
            new MenuAdapter.MenuItem("Friends", R.mipmap.ic_drawer_friends, FragmentType.Friends),
            new MenuAdapter.MenuItem("Search", R.mipmap.ic_menu_search, FragmentType.Search),
            new MenuAdapter.MenuItem("Settings", R.mipmap.ic_drawer_settings, FragmentType.Settings)
    };

    private MnApp _app;

    private UserSettings _userSettings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drawer);
        getWindow().setStatusBarColor(0x00ffffff);
        ButterKnife.inject(this);


        if (_toolbar != null) {
            setSupportActionBar(_toolbar);
        }

        _app = (MnApp) getApplication();
        if (!_app.hasAccessToken()) {
            Intent intent = new Intent(this, WebActivity.class);
            startActivityForResult(intent, _loginRequestCode);
        } else {
            _userSettings = _app.getUserSettings();

            ActionBarDrawerToggle _drawerToggle = new ActionBarDrawerToggle(this, _drawer, _toolbar, R.string.drawer_open, R.string.drawer_close) {
                @Override
                public void onDrawerOpened(View drawerView) {
                    super.onDrawerOpened(drawerView);
                }

                @Override
                public void onDrawerClosed(View drawerView) {
                    super.onDrawerClosed(drawerView);
                    invalidateOptionsMenu();
                }

            };

            _drawer.setDrawerListener(_drawerToggle);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
            _drawerToggle.syncState();


            _menu.setAdapter(new MenuAdapter(this, items));
            if (_userSettings != null) {
                setHeaderView(_userSettings.getUser());
            }

            _menu.setOnItemClickListener(this);
            openInitialFragment();
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Bundle extra = data.getExtras();
        if (extra != null) {
            String accessToken = extra.getString(WebActivity.TOKEN_KEY);
        }

        recreate();
    }

    private void setHeaderView(UserSettings.User user) {
        View header;
        header = LayoutInflater.from(this).inflate(R.layout.drawer_header_item, (ViewGroup) _menu.getRootView(), false);
        RoundedImageView avatar = (RoundedImageView) header.findViewById(R.id.avatar);
        Picasso.with(this).load(user.getAvatarUrl()).into(avatar);
        TextView username = (TextView) header.findViewById(R.id.username);
        username.setText(user.getUsername());
        TextView name = (TextView) header.findViewById(R.id.name);
        name.setText(user.getName());
        _menu.addHeaderView(header);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_drawer, menu);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        switch (_currentFragment) {
            case Movienights:
                menu.clear();
                getMenuInflater().inflate(R.menu.menu_movienight_fragment, menu);
                break;
            case Search:
                menu.clear();
                getMenuInflater().inflate(R.menu.menu_fragment_search, menu);
                break;
            default:
                menu.clear();
                getMenuInflater().inflate(R.menu.menu_drawer, menu);
                break;
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        switch (id) {
            case R.id.action_add:
                openNewMovienightFragment();
                break;
            case R.id.action_settings:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void openNewMovienightFragment() {
        FrameLayout overlayLayout = new FrameLayout(this);
        overlayLayout.setId(com.richardkoster.movienight.utils.ViewUtils.generateViewId());
        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT);

        getContentView().addView(overlayLayout, params);
        CreateMovienightFragment fragment = new CreateMovienightFragment();
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.add(overlayLayout.getId(), fragment);
        transaction.commit();

        overlayLayout.setAlpha(0);
        // Animate view to the top.
        overlayLayout.animate().alpha(1).setDuration(200).start();
    }

    public FrameLayout getContentView() {
        return (FrameLayout) findViewById(android.R.id.content);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        String title;
        if (position == 0) {
            title = _app.getUserSettings().getUser().getUsername();
        } else {
            title = items[position - 1]._name;
        }
        SpannableString sp = new SpannableString(title);
        sp.setSpan(new TypefaceSpan(this, "Museo-Sans-700.otf"), 0, title.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        setTitle(sp);
        _menu.setItemChecked(position - 1, true);
        switch (position) {
            case 1:
                MovienightsFragment mnFragment = new MovienightsFragment();
                getFragmentManager().beginTransaction()
                        .replace(R.id.content_frame, mnFragment)
                        .commit();
                _drawer.closeDrawer(_menu);
                supportInvalidateOptionsMenu();
                _currentFragment = FragmentType.Movienights;
                break;
            case 0:
            case 2:
                ListsFragment fragment = new ListsFragment();
                getFragmentManager().beginTransaction()
                        .replace(R.id.content_frame, fragment)
                        .commit();
                _currentFragment = FragmentType.Lists;
                supportInvalidateOptionsMenu();
                _drawer.closeDrawer(_menu);
                break;
            case 3:
                FriendsFragment friendsFragment = new FriendsFragment();
                getFragmentManager().beginTransaction()
                        .replace(R.id.content_frame, friendsFragment)
                        .commit();
                _currentFragment = FragmentType.Friends;
                supportInvalidateOptionsMenu();
                _drawer.closeDrawer(_menu);
                break;
            case 4:
                SearchFragment searchFragment = new SearchFragment();
                getFragmentManager().beginTransaction()
                        .replace(R.id.content_frame, searchFragment)
                        .commit();
                _currentFragment = FragmentType.Search;
                invalidateOptionsMenu();
                _drawer.closeDrawer(_menu);
                break;
            case 5:
                SettingsFragment watchedFragment = new SettingsFragment();
                getFragmentManager().beginTransaction()
                        .replace(R.id.content_frame, watchedFragment)
                        .commit();
                _currentFragment = FragmentType.Lists;
                supportInvalidateOptionsMenu();
                _drawer.closeDrawer(_menu);
                break;
        }
    }

    public void openInitialFragment() {
        MovienightsFragment fragment = new MovienightsFragment();
        getFragmentManager().beginTransaction()
                .replace(R.id.content_frame, fragment)
                .commit();

        _menu.setItemChecked(0, true);
        String title = (items[0])._name;
        SpannableString sp = new SpannableString(title);
        sp.setSpan(new TypefaceSpan(this, "Museo-Sans-700.otf"), 0, title.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        setTitle(sp);
        _currentFragment = FragmentType.Movienights;
    }
}
