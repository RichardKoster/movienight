package com.richardkoster.movienight.trakt.models.media;

import com.google.gson.annotations.SerializedName;

public class ImageCollection {

    @SerializedName("fanart")
    private ArtImage _fanart;
    @SerializedName("poster")
    private ArtImage _poster;
    @SerializedName("logo")
    private ArtImage _logo;
    @SerializedName("clearart")
    private ArtImage _clearart;
    @SerializedName("banner")
    private ArtImage _banner;
    @SerializedName("thumb")
    private ArtImage _thumb;
    @SerializedName("avatar")
    private ArtImage _avatar;

    public ArtImage getFanart() {
        return _fanart;
    }

    public ArtImage getPoster() {
        return _poster;
    }

    public ArtImage getAvatar() {
        return _avatar;
    }
}
