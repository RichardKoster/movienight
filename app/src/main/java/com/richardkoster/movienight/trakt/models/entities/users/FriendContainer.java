package com.richardkoster.movienight.trakt.models.entities.users;

import com.google.gson.annotations.SerializedName;

public class FriendContainer {

    @SerializedName("followed_at")
    private String _followedAt;
    @SerializedName("user")
    private UserSettings.User _user;

    public String getFollowedDate() {
        return _followedAt;
    }

    public UserSettings.User getUser() {
        return _user;
    }
}
