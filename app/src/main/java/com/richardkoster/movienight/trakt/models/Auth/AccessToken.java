package com.richardkoster.movienight.trakt.models.auth;

import com.google.gson.annotations.SerializedName;

public class AccessToken {

    @SerializedName("access_token")
    private String _accessToken;
    @SerializedName("token_type")
    private String _tokenType;
    @SerializedName("expires_in")
    private int _expiresIn;
    @SerializedName("refresh_token")
    private String _refreshToken;
    @SerializedName("scope")
    private String _scope;

    public String getAccessToken() {
        return _accessToken;
    }
}
