package com.richardkoster.movienight.trakt.api;

import com.richardkoster.movienight.app.MnApp;
import com.richardkoster.movienight.trakt.models.entities.movies.Movie;
import com.richardkoster.movienight.trakt.models.entities.movies.MovieContainer;
import com.richardkoster.movienight.trakt.models.entities.movies.HistorySync;
import com.richardkoster.movienight.trakt.models.entities.movies.WatchlistSync;
import com.richardkoster.movienight.trakt.models.entities.users.FriendContainer;
import com.richardkoster.movienight.trakt.models.entities.users.UserSettings;
import com.richardkoster.movienight.trakt.models.search.SearchResult;

import java.util.ArrayList;
import java.util.List;

import retrofit.Callback;
import retrofit.RequestInterceptor;
import retrofit.RestAdapter;
import retrofit.client.Response;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;
import retrofit.http.Path;
import retrofit.http.Query;

public class TraktApi {

    private static String endpoint = TraktAuthApi.BASE_URL;
    public final static String CLIENT_ID = "3e76837c8b7ce0af6e7e92a1205fa43988362e85e50ffa0971e8d5126ec010ff";
    public final static String CLIENT_SECRET = "3312ce244322148dfd773c170da948dfe916773219da363a7519f540c1ae8dab";

    public final static String SEARCH_MOVIE = "movie";

    public static Trakt getApi(final MnApp mnApp) {
        RestAdapter adapter = new RestAdapter.Builder()
                .setEndpoint(endpoint)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setRequestInterceptor(new RequestInterceptor() {
                    @Override
                    public void intercept(RequestFacade request) {
                        request.addHeader("Content-Type", "application/json");
                        request.addHeader("trakt-api-key", CLIENT_ID);
                        request.addHeader("trakt-api-version", "2");
                        request.addHeader("Authorization", String.format("Bearer %s", mnApp.getAccessToken().getAccessToken()));
                    }
                })
                .build();

        return adapter.create(Trakt.class);
    }

    public interface Trakt {
        @GET("/users/settings")
        public void getCurrentUser(Callback<UserSettings> cb);

        @GET("/users/{user}/watchlist/movies?extended=full,images")
        public void getWatchlistForUser(@Path("user") String user, Callback<List<MovieContainer>> cb);

        @GET("/users/{user}/watched/movies?extended=full,images")
        public void getWatchedForUser(@Path("user") String user, Callback<List<MovieContainer>> cb);

        @GET("/users/{user}/followers?extended=full,images")
        public void getFollowersForUser(@Path("user") String user, Callback<List<FriendContainer>> cb);

        @GET("/users/{user}/following?extended=full,images")
        public void getFollowingForUser(@Path("user") String user, Callback<List<FriendContainer>> cb);

        @GET("/users/{user}/friends?extended=full,images")
        public void getFriendsForUser(@Path("user") String user, Callback<List<FriendContainer>> cb);

        @GET("/movies/{id}?extended=full,images")
        void getMovie(@Path("id") String slug, Callback<Movie> cb);

        @POST("/sync/history")
        void addToWatched(@Body HistorySync sync, Callback<Response> cb);

        @POST("/sync/watchlist")
        void addToWatchlist(@Body WatchlistSync sync, Callback<Response> cb);

        @POST("/sync/history/remove")
        void removeFromHistory(@Body WatchlistSync sync, Callback<Response> cb);

        @GET("/search")
        void searchForMovie(@Query("query") String searchQuery, @Query("type") String searchType, Callback<ArrayList<SearchResult.MovieResult>> cb);

        @GET("/search")
        void searchForMovieWithYear(@Query("query") String searchQuery, @Query("type") String searchType, @Query("year") String year, Callback<ArrayList<SearchResult.MovieResult>> cb);
    }
}
