package com.richardkoster.movienight.trakt.models.media;

import com.google.gson.annotations.SerializedName;

public class ArtImage {

    public final static int FULL = 0;
    public final static int MEDIUM = 1;
    public final static int THUMB = 2;

    @SerializedName("full")
    private String _full;
    @SerializedName("medium")
    private String _medium;
    @SerializedName("thumb")
    private String _thumb;

    public String getImage(int size) {
        switch (size) {
            case FULL:
                return _full;
            case MEDIUM:
                return _medium;
            case THUMB:
                return _thumb;
            default:
                return _full;
        }
    }

    public String getFull() {
        return _full;
    }

    public String getMedium() {
        return _medium;
    }
}
