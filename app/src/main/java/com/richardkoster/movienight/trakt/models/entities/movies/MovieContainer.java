package com.richardkoster.movienight.trakt.models.entities.movies;

import com.google.gson.annotations.SerializedName;

public class MovieContainer {

    @SerializedName("listed_at")
    String _listedAt;
    @SerializedName("type")
    String _type;
    @SerializedName("plays")
    int _plays;
    @SerializedName("last_watched_at")
    String _lastWatchedAt;
    @SerializedName("movie")
    Movie _movie;

    public Movie getMovie() {
        return _movie;
    }
}
