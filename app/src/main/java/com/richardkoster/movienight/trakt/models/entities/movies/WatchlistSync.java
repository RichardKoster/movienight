package com.richardkoster.movienight.trakt.models.entities.movies;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class WatchlistSync {

    @SerializedName("movies")
    ArrayList<WatchlistSyncMovie> _movies;

    public WatchlistSync(){
        _movies = new ArrayList<>();
    }

    public void setSyncHistoryMovie(WatchlistSyncMovie movie){
        _movies.add(movie);
    }

    public static class WatchlistSyncMovie {

        @SerializedName("ids")
        private Movie.Ids _ids;

        public WatchlistSyncMovie(Movie.Ids ids){
            _ids = ids;
        }
    }

}
