package com.richardkoster.movienight.trakt.models.entities.movies;

import com.google.gson.annotations.SerializedName;
import com.richardkoster.movienight.trakt.models.media.ImageCollection;

import java.util.List;

public class Movie {

    public final static int NOTHING = 0;
    public final static int WATCHLIST = 1;
    public final static int WATCHED = 2;

    @SerializedName("ids")
    private Ids _ids;
    @SerializedName("title")
    private String _title;
    @SerializedName("year")
    private int _year;
    @SerializedName("tagline")
    private String _tagline;
    @SerializedName("images")
    private ImageCollection _images;
    @SerializedName("overview")
    private String _overview;
    @SerializedName("released")
    private String _releaseDate;
    @SerializedName("runtime")
    private int _runTime;
    @SerializedName("trailer")
    private String _trailerUtl;
    @SerializedName("rating")
    private double _rating;
    @SerializedName("votes")
    private int _votes;
    @SerializedName("genres")
    private List<String> _genres;

    private int _status=NOTHING;

    public Ids getIds() {
        return _ids;
    }

    public String getTitle() {
        return _title;
    }

    public int geYear() {
        return _year;
    }

    public String getTagline() {
        return _tagline;
    }

    public ImageCollection getImages() {
        return _images;
    }

    public String getOverview() {
        return _overview;
    }

    public String getReleaseDate() {
        return _releaseDate;
    }

    public int getRunTime() {
        return _runTime;
    }

    public String getTrailerUtl() {
        return _trailerUtl;
    }

    public double getRating() {
        return _rating;
    }

    public int getVotes() {
        return _votes;
    }

    public List<String> getGenres() {
        return _genres;
    }

    public class Ids {
        @SerializedName("trakt")
        public int _traktId;
        @SerializedName("slug")
        public String _traktSlug;
        @SerializedName("imdb")
        public String _imdbId;
        @SerializedName("tmdb")
        public int _tmdbId;
    }

    public String getReleaseYear(){
        return Integer.toString(_year);
    }

    public String getMetaString() {
        String metaString = "";
        if (getReleaseYear() != null || getReleaseYear() != "") {
            metaString += getReleaseYear();
        }
        if (_genres.size() > 0) {
            metaString += " • ";
            String genre = _genres.get(0);
            String capitalize = genre.substring(0, 1).toUpperCase() + genre.substring(1).toLowerCase();
            metaString += capitalize;
        }
        if (_genres.size() > 1) {
            for (int i = 1; i < _genres.size(); i++) {
                metaString += " - ";
                String genre = _genres.get(i);
                String capitalize = genre.substring(0, 1).toUpperCase() + genre.substring(1).toLowerCase();
                metaString += capitalize;
            }
        }
        if (getRunTime() != 0) {
            metaString += " • ";
            metaString += Integer.toString(getRunTime()) + "min";
        }
        return metaString;
    }

    public void setStatus(int status){
        _status = status;
    }

    public int getStatus(){
        return _status;
    }
}
