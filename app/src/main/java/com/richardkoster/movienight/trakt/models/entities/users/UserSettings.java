package com.richardkoster.movienight.trakt.models.entities.users;

import com.google.gson.annotations.SerializedName;
import com.richardkoster.movienight.trakt.models.media.ImageCollection;

public class UserSettings {

    @SerializedName("user")
    private User _user;
    @SerializedName("account")
    private Account _account;
    @SerializedName("connections")
    private Connections _connections;
    @SerializedName("sharing_text")
    private SharingTexts _sharingText;

    public User getUser() {
        return _user;
    }

    public Account getAccount() {
        return _account;
    }

    public Connections getConnections() {
        return _connections;
    }

    public SharingTexts getSharingText() {
        return _sharingText;
    }

    public class User {

        @SerializedName("username")
        private String _username;
        @SerializedName("name")
        private String _name;
        @SerializedName("location")
        private String _location;
        @SerializedName("about")
        private String _about;
        @SerializedName("gender")
        private String _gender;
        @SerializedName("age")
        private int _age;
        @SerializedName("images")
        private ImageCollection _images;

        public String getUsername() {
            return _username;
        }

        public String getAvatarUrl() {
            return _images.getAvatar().getFull();
        }

        public String getName() {
            return _name;
        }

        public String getLocation() {
            return _location;
        }

        public String getAbout() {
            return _about;
        }

        public String getGender() {
            return _gender;
        }

        public int getAge() {
            return _age;
        }
    }

    public class Account {
        @SerializedName("timezone")
        private String _timezone;
        @SerializedName("cover_image")
        private String _coverImage;

        public String getTimezone() {
            return _timezone;
        }

        public String getCoverImage() {
            return _coverImage;
        }
    }

    public class Connections {
        @SerializedName("facebook")
        private boolean _isConnectedToFacebook;
        @SerializedName("twitter")
        private boolean _isConnectedToTwitter;
        @SerializedName("google")
        private boolean _isConnectedToGoogle;
        @SerializedName("tumblr")
        private boolean _isConnectedToTumblr;

        public boolean isConnectedToFacebook() {
            return _isConnectedToFacebook;
        }

        public boolean isConnectedToTwitter() {
            return _isConnectedToTwitter;
        }

        public boolean isConnectedToGoogle() {
            return _isConnectedToGoogle;
        }

        public boolean isConnectedToTumblr() {
            return _isConnectedToTumblr;
        }
    }

    public class SharingTexts {

        @SerializedName("watching")
        private String _watching;
        @SerializedName("watched")
        private String _watched;

        public String getWatching() {
            return _watching;
        }

        public String getWatched() {
            return _watched;
        }
    }
}
