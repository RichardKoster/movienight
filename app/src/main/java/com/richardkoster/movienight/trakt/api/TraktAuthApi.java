package com.richardkoster.movienight.trakt.api;

import com.richardkoster.movienight.trakt.models.auth.AccessToken;
import com.richardkoster.movienight.trakt.models.auth.RequestToken;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.http.Body;
import retrofit.http.POST;

public class TraktAuthApi {

    public final static String BASE_URL = "https://api-v2launch.trakt.tv";

    public static TraktAuth getAuthApi() {
        RestAdapter adapter = new RestAdapter.Builder()
                .setEndpoint(BASE_URL)
                .build();

        return adapter.create(TraktAuth.class);
    }

    public interface TraktAuth {
        @POST("/oauth/token")
        public void getAccessToken(@Body RequestToken requestToken, Callback<AccessToken> cb);
    }
}
