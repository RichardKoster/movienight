package com.richardkoster.movienight.trakt.models.entities.movies;

import com.google.gson.annotations.SerializedName;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import java.util.ArrayList;

public class HistorySync {

    @SerializedName("movies")
    ArrayList<SyncHistoryMovie> _movies;

    public HistorySync(){
        _movies = new ArrayList<>();
    }

    public void setSyncHistoryMovie(SyncHistoryMovie movie){
        _movies.add(movie);
    }

    public static class SyncHistoryMovie {

        @SerializedName("ids")
        private Movie.Ids _ids;
        @SerializedName("watched_at")
        private String _watchedAt;

        public SyncHistoryMovie(Movie.Ids ids){
            _ids = ids;
            _watchedAt = new DateTime(DateTimeZone.UTC).toString();
        }
    }

}
