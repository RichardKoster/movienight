package com.richardkoster.movienight.trakt.models.search;

import com.google.gson.annotations.SerializedName;
import com.richardkoster.movienight.trakt.models.entities.movies.Movie;

import java.util.ArrayList;

public class SearchResult {

    public static ArrayList<Movie> getMovieList(ArrayList<MovieResult> results){
        ArrayList<Movie> movies = new ArrayList();
        for (int i = 0; i < results.size(); i++) {
             movies.add(results.get(i).movie);
        }
        return movies;
    }

    public static class MovieResult{

        @SerializedName("type")
        String type;
        @SerializedName("score")
        float score;
        @SerializedName("movie")
        Movie movie;
    }
}
