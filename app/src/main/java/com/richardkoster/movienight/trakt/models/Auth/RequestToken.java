package com.richardkoster.movienight.trakt.models.auth;

import com.google.gson.annotations.SerializedName;
import com.richardkoster.movienight.trakt.api.TraktApi;

public class RequestToken {

    @SerializedName("code")
    private String _code;
    @SerializedName("client_id")
    private String _clientId;
    @SerializedName("client_secret")
    private String _clientSecret;
    @SerializedName("redirect_uri")
    private String _redirectUri;
    @SerializedName("grant_type")
    private String _grantType;

    public RequestToken(String code, String redirectUri, String grantType) {
        _code = code;
        _redirectUri = redirectUri;
        _grantType = grantType;
        _clientId = TraktApi.CLIENT_ID;
        _clientSecret = TraktApi.CLIENT_SECRET;
    }
}
