package com.richardkoster.movienight;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.PathInterpolator;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;

import com.richardkoster.movienight.app.MnApp;
import com.richardkoster.movienight.trakt.api.TraktApi;
import com.richardkoster.movienight.trakt.api.TraktAuthApi;
import com.richardkoster.movienight.trakt.models.auth.AccessToken;
import com.richardkoster.movienight.trakt.models.auth.RequestToken;

import butterknife.ButterKnife;
import butterknife.InjectView;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import ui.Button;

public class WebActivity extends Activity implements MnApp.OnSavedUserInfoListener, Button.OnClickListener {

    @InjectView(R.id.webview)
    WebView _webview;
    @InjectView(R.id.cover)
    FrameLayout _cover;
    @InjectView(R.id.connect_btn)
    Button _connectButton;

    private MnApp _mnApp;

    public static final String TOKEN_KEY = "TOKEN";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web);

        ButterKnife.inject(this);

        _mnApp = (MnApp) getApplication();
        _mnApp.setOnSavedUserInfoListener(this);

        _webview.loadUrl("https://trakt.tv/oauth/authorize?response_type=code&client_id=" + TraktApi.CLIENT_ID + "&redirect_uri=com.richardkoster.movienight://login");
        WebViewClient client = new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                Uri uri = Uri.parse(url);
                if (!uri.getScheme().equals("com.richardkoster.movienight")) {
                    return false;
                }

                String code = uri.getQueryParameter("code");
                getAccessToken(code);
                return true;
            }
        };
        _webview.setWebViewClient(client);

        _connectButton.setOnClickListener(this);
    }

    private void getAccessToken(final String code) {
        RequestToken reqToken = new RequestToken(code, "com.richardkoster.movienight://login", "authorization_code");
        TraktAuthApi.getAuthApi().getAccessToken(reqToken, new Callback<AccessToken>() {
            @Override
            public void success(AccessToken accessToken, Response response) {
                _mnApp.setAccessToken(accessToken);
            }

            @Override
            public void failure(RetrofitError error) {
                Log.d("Retro", error.getLocalizedMessage());
                if (error.getResponse().getStatus() == 504) {
                    getAccessToken(code);
                }
            }
        });
    }

    @Override
    public void didSaveUser() {
        Intent intent = new Intent();
        intent.putExtra(TOKEN_KEY, _mnApp.getAccessToken().getAccessToken());
        setResult(RESULT_OK, intent);
        finish();
//        Intent intent = new Intent(this, DrawerActivity.class);
//        startActivity(intent);
    }

    @Override
    public void onClick(View v) {
        Animation fadeOut = new AlphaAnimation(1, 0);
        fadeOut.setInterpolator(new PathInterpolator(0.42f, 0, 0.58f, 1));
        fadeOut.setDuration(200);
        _cover.startAnimation(fadeOut);
        _cover.setVisibility(View.GONE);
    }
}
