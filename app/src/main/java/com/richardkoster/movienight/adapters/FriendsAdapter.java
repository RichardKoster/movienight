package com.richardkoster.movienight.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.richardkoster.movienight.R;
import com.richardkoster.movienight.adapters.viewholders.FriendViewHolder;
import com.richardkoster.movienight.trakt.models.entities.users.FriendContainer;

import java.util.Collections;
import java.util.List;

public class FriendsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    OnItemTappedListener _l;

    List<FriendContainer> _dataset;

    public FriendsAdapter(OnItemTappedListener listener) {
        _l = listener;
        _dataset = Collections.emptyList();
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final FriendViewHolder fvh = new FriendViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.viewholder_friend, parent, false), new FriendViewHolder.OnClickListener() {
            @Override
            public void onClick(FriendViewHolder vh) {
                _l.onTap(vh, vh.getLayoutPosition());
            }
        });
        return fvh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((FriendViewHolder) holder).setFriend(_dataset.get(position));
    }

    @Override
    public int getItemCount() {
        return _dataset.size();
    }

    public void updateDataset(List<FriendContainer> dataset) {
        _dataset = dataset;
        notifyDataSetChanged();
    }

    public interface OnItemTappedListener {
        void onTap(FriendViewHolder vh, int position);
    }
}
