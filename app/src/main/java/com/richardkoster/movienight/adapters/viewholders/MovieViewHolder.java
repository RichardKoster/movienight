package com.richardkoster.movienight.adapters.viewholders;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.richardkoster.movienight.R;
import com.richardkoster.movienight.trakt.models.entities.movies.Movie;
import com.richardkoster.movienight.trakt.models.media.ArtImage;
import com.squareup.picasso.Picasso;

import butterknife.ButterKnife;
import butterknife.InjectView;
import ui.RoundedImageView;
import ui.TextView;

public class MovieViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    @InjectView(R.id.poster)
    RoundedImageView _poster;
    @InjectView(R.id.title)
    TextView _title;
    @InjectView(R.id.year)
    TextView _year;

    Context _context;

    MovieViewHolderClicks _l;

    public MovieViewHolder(View itemView, MovieViewHolderClicks listener) {
        super(itemView);
        _l = listener;
        ButterKnife.inject(this, itemView);
        _context = itemView.getContext();
        itemView.setOnClickListener(this);
    }

    public void setMovie(Movie movie) {
        Picasso.with(_context).load(movie.getImages().getPoster().getImage(ArtImage.THUMB)).into(_poster);
        _title.setText(movie.getTitle());
        _year.setText(movie.getReleaseYear());
    }


    @Override
    public void onClick(View v) {
        _l.onClick(this);
    }

    public interface MovieViewHolderClicks {
        void onClick(MovieViewHolder vh);
    }
}
