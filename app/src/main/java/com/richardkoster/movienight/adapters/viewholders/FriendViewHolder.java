package com.richardkoster.movienight.adapters.viewholders;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.richardkoster.movienight.R;
import com.richardkoster.movienight.trakt.models.entities.users.FriendContainer;
import com.richardkoster.movienight.trakt.models.entities.users.UserSettings;
import com.squareup.picasso.Picasso;

import butterknife.ButterKnife;
import butterknife.InjectView;
import ui.RoundedImageView;
import ui.TextView;

public class FriendViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

    OnClickListener _l;

    @InjectView(R.id.avatar)
    RoundedImageView _avatar;
    @InjectView(R.id.username)
    TextView _username;
    @InjectView(R.id.name)
    TextView _name;

    Context _context;

    public FriendViewHolder(View itemView, FriendViewHolder.OnClickListener listener) {
        super(itemView);
        _context = itemView.getContext();
        _l = listener;
        itemView.setOnClickListener(this);
        ButterKnife.inject(this, itemView);
    }

    public void setFriend(FriendContainer container) {
        UserSettings.User user = container.getUser();
        Picasso.with(_context).load(user.getAvatarUrl()).into(_avatar);
        _username.setText(user.getUsername());
        _name.setText(user.getName());
    }

    @Override
    public void onClick(View v) {
        _l.onClick(this);
    }

    public interface OnClickListener {
        void onClick(FriendViewHolder vh);
    }
}
