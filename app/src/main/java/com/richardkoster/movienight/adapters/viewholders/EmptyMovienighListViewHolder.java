package com.richardkoster.movienight.adapters.viewholders;

import android.support.v7.widget.RecyclerView;
import android.view.View;

public class EmptyMovienighListViewHolder extends RecyclerView.ViewHolder {
    public EmptyMovienighListViewHolder(View itemView) {
        super(itemView);
    }
}
