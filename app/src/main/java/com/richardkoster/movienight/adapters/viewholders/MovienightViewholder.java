package com.richardkoster.movienight.adapters.viewholders;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.richardkoster.movienight.R;
import com.richardkoster.movienight.api.models.Movienight;
import com.squareup.picasso.Picasso;

import org.joda.time.DateTime;

import butterknife.ButterKnife;
import butterknife.InjectView;
import ui.RoundedImageView;
import ui.TextView;

public class MovienightViewholder extends RecyclerView.ViewHolder implements View.OnClickListener {
    @InjectView(R.id.poster)
    RoundedImageView _poster;
    @InjectView(R.id.title)
    TextView _title;
    @InjectView(R.id.company)
    TextView _company;
    @InjectView(R.id.date_number)
    TextView _dateNumber;
    @InjectView(R.id.date_month)
    TextView _dateMonth;

    private Context _context;
    OnTappedListener _l;

    public MovienightViewholder(View itemView, final OnTappedListener listener) {
        super(itemView);
        _l = listener;
        ButterKnife.inject(this, itemView);
        _context = itemView.getContext();
        itemView.setOnClickListener(this);
    }

    public void populate(Movienight movienight) {
        Picasso.with(_context).load("https://walter.trakt.us/images/movies/000/148/553/posters/thumb/a1368995ea.jpg").into(_poster);
        _title.setText("Hot Tub Time Machine 2");
        _company.setText(String.format("With %s", "Kevin, Alex and Marie"));
        DateTime dt  = new DateTime();
        DateTime now = new DateTime();
        if(dt.isBefore(now.plusWeeks(1))){
            _dateMonth.setTextColor(0xffF44336);
            _dateNumber.setTextColor(0xffF44336);
        }
        _dateNumber.setText(Integer.toString(dt.getDayOfMonth()));
        _dateMonth.setText(dt.monthOfYear().getAsShortText());
    }

    @Override
    public void onClick(View v) {
        _l.onTap(this);
    }

    public interface OnTappedListener{
        void onTap(MovienightViewholder vh);
    }

}
