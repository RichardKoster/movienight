package com.richardkoster.movienight.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.richardkoster.movienight.R;
import com.richardkoster.movienight.adapters.viewholders.MovieViewHolder;
import com.richardkoster.movienight.adapters.viewholders.SimpleViewHolder;
import com.richardkoster.movienight.trakt.models.entities.movies.Movie;

import java.util.Collections;
import java.util.List;

public class SearchAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final static int TYPE_MOVIE =1;
    private final static int TYPE_EMPTY=2;
    private final static int TYPE_END=3;

    List<Movie> dataset;
    OnItemTappedListener _l;

    @Override
    public int getItemViewType(int position) {
        if(dataset.size()==0){
            return TYPE_EMPTY;
        }
        else if(position==dataset.size()){
            return TYPE_END;
        }
        return TYPE_MOVIE;
    }

    public SearchAdapter() {
        dataset = Collections.emptyList();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType){
            case TYPE_MOVIE:
                return new MovieViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.viewholder_list_movie, parent, false), new MovieViewHolder.MovieViewHolderClicks() {
                    @Override
                    public void onClick(MovieViewHolder vh) {
                        _l.onTap(vh, vh.getLayoutPosition());
                    }
                });
            case TYPE_END:
                return new SimpleViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.viewholder_end, parent, false));
            case TYPE_EMPTY:
                return new SimpleViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.viewholder_search_empty, parent, false));
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if(position<dataset.size()){
            ((MovieViewHolder) holder).setMovie(dataset.get(position));
        }
    }

    @Override
    public int getItemCount() {
        return dataset.size()+1;
    }

    public void updateDataset(List<Movie> newDataset) {
        dataset = newDataset;
        notifyDataSetChanged();
    }

    public void setOnItemTappedListener(OnItemTappedListener listener){
        _l = listener;
    }

    public interface OnItemTappedListener{
        void onTap(RecyclerView.ViewHolder vh, int position);
    }
}
