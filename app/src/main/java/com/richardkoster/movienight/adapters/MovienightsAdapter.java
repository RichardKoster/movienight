package com.richardkoster.movienight.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.richardkoster.movienight.R;
import com.richardkoster.movienight.adapters.viewholders.EmptyMovienighListViewHolder;
import com.richardkoster.movienight.adapters.viewholders.MovienightViewholder;
import com.richardkoster.movienight.adapters.viewholders.MultipeMoviesnightViewholder;
import com.richardkoster.movienight.adapters.viewholders.SimpleViewHolder;
import com.richardkoster.movienight.api.models.Movienight;

import java.util.Collections;
import java.util.List;

public class MovienightsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final static int TYPE_MOVIENIGHT = 0;
    private final static int TYPE_END = 1;
    private final static int TYPE_EMPTY = 2;
    private final static int TYPE_MULTI = 3;

    private List<Movienight> _dataset;

    public MovienightsAdapter() {
        _dataset = Collections.emptyList();
    }

    @Override
    public int getItemViewType(int position) {
        if (_dataset.size() == 0) {
            return TYPE_EMPTY;
        }
        if (position == _dataset.size()) {
            return TYPE_END;
        }
        if (_dataset.get(position).hasMultiple()) {
            return TYPE_MULTI;
        }
        return TYPE_MOVIENIGHT;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case TYPE_END:
                return new SimpleViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.viewholder_end, parent, false));
            case TYPE_EMPTY:
                return new EmptyMovienighListViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.viewholder_empty_movienights_list, parent, false));
            case TYPE_MOVIENIGHT:
                 return new MovienightViewholder(LayoutInflater.from(parent.getContext()).inflate(R.layout.viewholder_movienight, parent, false), new MovienightViewholder.OnTappedListener() {
                    @Override
                    public void onTap(MovienightViewholder vh) {
                        _l.onItemTapped(vh, vh.getLayoutPosition());
                    }
                });
            case TYPE_MULTI:
                return new MultipeMoviesnightViewholder(LayoutInflater.from(parent.getContext()).inflate(R.layout.viewholder_multimovienight, parent, false));
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (position < _dataset.size()) {
            ((MovienightViewholder) holder).populate(_dataset.get(position));
        }
    }

    @Override
    public int getItemCount() {
        return _dataset.size() + 1;
    }

    public void updateDataset(List<Movienight> newData) {
        _dataset = newData;
        notifyDataSetChanged();
    }

    OnItemTappedListener _l;

    public void setOnItemTappedListener(OnItemTappedListener listener){
        _l = listener;
    }

    public interface OnItemTappedListener{
        void onItemTapped(MovienightViewholder vh, int position);
    }
}
