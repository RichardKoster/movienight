package com.richardkoster.movienight.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;

import com.richardkoster.movienight.R;
import com.richardkoster.movienight.fragments.FragmentType;

import ui.TextView;

public class MenuAdapter extends ArrayAdapter<MenuAdapter.MenuItem> {

    public MenuAdapter(Context context, MenuItem[] menuItems) {
        super(context, R.layout.drawer_item, menuItems);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        MenuItem item = getItem(position);
        convertView = LayoutInflater.from(getContext()).inflate(R.layout.drawer_item, parent, false);
        ImageView icon = (ImageView) convertView.findViewById(R.id.icon);
        icon.setImageDrawable(getContext().getDrawable(item._icon));
        TextView name = (TextView) convertView.findViewById(R.id.name);
        name.setText(item._name);
        return convertView;
    }

    public static class MenuItem {

        public String _name;
        public int _icon;
        public FragmentType _target;

        public MenuItem(String name, int icon, FragmentType target) {
            _name = name;
            _icon = icon;
            _target = target;
        }
    }
}
