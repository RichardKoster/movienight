package com.richardkoster.movienight.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.richardkoster.movienight.R;
import com.richardkoster.movienight.adapters.viewholders.MovieViewHolder;
import com.richardkoster.movienight.trakt.models.entities.movies.Movie;

import java.util.Collections;
import java.util.List;

public class MovieAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    List<Movie> dataset;
    OnItemTappedListener _l;

    public MovieAdapter() {
        dataset = Collections.emptyList();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MovieViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.viewholder_list_movie, parent, false), new MovieViewHolder.MovieViewHolderClicks() {
            @Override
            public void onClick(MovieViewHolder vh) {
                _l.onTap(vh, vh.getLayoutPosition());
            }
        });
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((MovieViewHolder) holder).setMovie(dataset.get(position));
    }

    @Override
    public int getItemCount() {
        return dataset.size();
    }

    public void updateDataset(List<Movie> newDataset) {
        dataset = newDataset;
        notifyDataSetChanged();
    }

    public void setOnItemTappedListener(OnItemTappedListener listener){
        _l = listener;
    }

    public interface OnItemTappedListener{
        void onTap(RecyclerView.ViewHolder vh, int position);
    }
}
