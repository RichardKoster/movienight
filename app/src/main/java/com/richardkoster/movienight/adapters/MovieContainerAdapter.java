package com.richardkoster.movienight.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.richardkoster.movienight.R;
import com.richardkoster.movienight.adapters.viewholders.MovieViewHolder;
import com.richardkoster.movienight.adapters.viewholders.SimpleViewHolder;
import com.richardkoster.movienight.trakt.models.entities.movies.MovieContainer;

import java.util.Collections;
import java.util.List;

public class MovieContainerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    List<MovieContainer> dataset;
    OnItemTappedListener _l;

    private final static int MOVIE_TYPE = 0;
    private final static int END_TYPE = 1;

    public MovieContainerAdapter(OnItemTappedListener listener) {
        dataset = Collections.emptyList();
        _l = listener;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == dataset.size() + 1) {
            return END_TYPE;
        }
        return MOVIE_TYPE;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case MOVIE_TYPE:
                MovieViewHolder v = new MovieViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.viewholder_list_movie, parent, false), new MovieViewHolder.MovieViewHolderClicks() {
                    @Override
                    public void onClick(MovieViewHolder vh) {
                        _l.onClick(vh, vh.getLayoutPosition());
                    }
                });
                return v;
            case END_TYPE:
                return new SimpleViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.viewholder_end, parent, false));
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ((MovieViewHolder) holder).setMovie(dataset.get(position).getMovie());
    }

    @Override
    public int getItemCount() {
        return dataset.size();
    }

    public void updateDataset(List<MovieContainer> newDataset) {
        dataset = newDataset;
        notifyDataSetChanged();
    }

    public interface OnItemTappedListener {
        void onClick(MovieViewHolder vh, int position);
    }
}
